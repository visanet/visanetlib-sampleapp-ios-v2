//
//  VisaNetConfigurationContext.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo on 9/3/14.
//  Copyright (c) 2014 VisaNet. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, VisaNetLanguage) {
    DEFAULT = 0,
    SPANISH = 1,
    ENGLISH = 2
};

typedef NS_ENUM(NSInteger, VisaNetCurrency) {
    PEN = 604,
    USD = 840
};

@interface VisaNetConfigurationContext : NSObject

// context
@property(nonatomic, copy, readwrite) NSString *merchantId;
@property(nonatomic, copy, readwrite) NSString *merchantName;
@property(nonatomic, copy, readwrite) NSString *accessKeyId;
@property(nonatomic, copy, readwrite) NSString *secretAccessKey;
@property(nonatomic, assign, readwrite) VisaNetLanguage language;
@property(nonatomic, assign, readwrite) VisaNetCurrency currency;
@property(nonatomic, copy, readwrite) NSString *userTokenId;
@property(nonatomic, assign, readwrite) BOOL isTesting;
@property(nonatomic, copy, readwrite) NSNumber *amount;
@property(nonatomic, copy, readwrite) NSString *transactionId;
@property(nonatomic, copy, readwrite) NSString *externalTransactionId;
@property(nonatomic, copy, readwrite) NSMutableDictionary *data;
@property(nonatomic, copy, readwrite) NSNumber *maxCardsPerToken;

// customer
@property(nonatomic, copy, readwrite) NSString *comment;
@property(nonatomic, copy, readwrite) NSString *customerFirstName;
@property(nonatomic, copy, readwrite) NSString *customerLastName;
@property(nonatomic, copy, readwrite) NSString *customerEmail;
@property(nonatomic, copy, readwrite) NSNumber *latitude;
@property(nonatomic, copy, readwrite) NSNumber *longitude;
@property(nonatomic, copy, readwrite) NSNumber *timeout;

// antifraud
@property(nonatomic, copy, readwrite) NSMutableDictionary *merchantDefinedData;

// profile
@property(nonatomic, copy, readwrite) NSString *profileId;

// list of cards
@property (nonatomic, copy, readwrite) NSMutableArray *cardArray;

- (void) checkVersion;
- (void) doProfile;
- (id) initWithTestingEnvironment: (BOOL) isTesting;
@end
