//
//  CardTransactionResponse.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo on 8/9/15.
//  Copyright (c) 2015 VisaNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardTransactionResponse : NSObject

@property(nonatomic, copy, readwrite) NSNumber *errorCode;
@property(nonatomic, copy, readwrite) NSString *errorMessage;
@property(nonatomic, copy, readwrite) NSString *transactionUUID;
@property(nonatomic, copy, readwrite) NSString *externalTransactionId;
@property(nonatomic, copy, readwrite) NSNumber *transactionDateTime;
@property(nonatomic, copy, readwrite) NSNumber *transactionDuraction;
@property(nonatomic, copy, readwrite) NSString *merchantId;
@property(nonatomic, copy, readwrite) NSString *userTokenId;
@property(nonatomic, copy, readwrite) NSString *aliasName;
@property(nonatomic, copy, readwrite) NSMutableDictionary *data;

- (void) putData: (id) value forKey: (NSString *) key;
- (id) retrieveData: (NSString *) key;

@end
