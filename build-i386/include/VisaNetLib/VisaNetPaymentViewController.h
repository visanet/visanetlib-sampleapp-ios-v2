//
//  VisaNeyPaymentViewController.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo @QuipuTech on 9/3/14.
//  Copyright (c) 2014 VisaNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisaNetPaymentInfo.h"
#import "VisaNetConfigurationContext.h"
#import "VisaNetPaymentDelegate.h"


@interface VisaNetPaymentViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate>
{
    id<VisaNetPaymentDelegate> _delegate;
}

@property(nonatomic, strong) id<VisaNetPaymentDelegate> delegate;

- (instancetype) initWithConfigurationContext: (VisaNetConfigurationContext *) context;

- (void)viewDidLoad;

- (void)didReceiveMemoryWarning;

- (void)setScrollEnable:(BOOL)enable;

@end
