//
//  main.m
//  VisaNetSampleApp
//
//  Created by Giancarlo Gallardo on 9/3/14.
//  Copyright (c) 2014 QuipuTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VisaNetAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VisaNetAppDelegate class]));
    }
}
