//
//  VisaNetPaymentInfo.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo on 9/4/14.
//  Copyright (c) 2014 VisaNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VisaNetPaymentInfo : NSObject

@property(nonatomic, copy, readwrite) NSString *paymentStatus;
@property(nonatomic, copy, readwrite) NSString *paymentDescription;
@property(nonatomic, copy, readwrite) NSString *transactionId;
@property(nonatomic, copy, readwrite) NSNumber *transactionDate;
@property(nonatomic, copy, readwrite) NSString *firstName;
@property(nonatomic, copy, readwrite) NSString *lastName;
@property(nonatomic, copy, readwrite) NSString *email;
@property(nonatomic, copy, readwrite) NSDictionary *data;

@end
