//
//  VisaNetViewController.h
//  VisaNetSampleApp
//
//  Created by Giancarlo Gallardo on 9/3/14.
//  Copyright (c) 2014 QuipuTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisaNetPaymentViewController.h"
#import "VisaNetCardListViewController.h"
#import "VisaNetCardInfo.h"
#import "VisaNetPaymentDelegate.h"

@interface VisaNetViewController : UIViewController<VisaNetPaymentDelegate>
{
    VisaNetPaymentViewController *paymentController;
    
    VisaNetCardListViewController *cardListController;
}
@end
