//
//  LoadingActivity.h
//  E-stateBook
//
//  Created by AlexWang on 2/15/15.
//  Copyright (c) 2015 TechTroll. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LoadingActivity : NSObject

+ (void)loadActivity:(UIView*)view;
+ (void)finishActivity:(UIView*)parent;

@end
