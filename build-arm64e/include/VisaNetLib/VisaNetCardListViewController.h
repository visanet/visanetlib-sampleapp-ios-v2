//
//  VisaNetCardListViewController.h
//  VisaNetLib
//
//  Created by AlexWang on 6/21/15.
//  Copyright (c) 2015 VisaNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisaNetCardInfo.h"
#import "VisaNetPaymentInfo.h"
#import "VisaNetConfigurationContext.h"
#import "VisaNetPaymentDelegate.h"


@interface VisaNetCardListViewController : UIViewController
{
    id<VisaNetPaymentDelegate> _delegate;
}

@property(nonatomic, strong) id<VisaNetPaymentDelegate> delegate;


- (instancetype) initWithConfigurationContext: (VisaNetConfigurationContext *) context;

@end
