//
//  CardTransactionRequest.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo on 8/9/15.
//  Copyright (c) 2015 VisaNet. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CardTransactionRequest : NSObject

@property(nonatomic, copy, readwrite) NSString *merchantId;
@property(nonatomic, copy, readwrite) NSString *purchaseNumber;
@property(nonatomic, copy, readwrite) NSString *promoCode;
@property(nonatomic, copy, readwrite) NSString *firstName;
@property(nonatomic, copy, readwrite) NSString *lastName;
@property(nonatomic, copy, readwrite) NSString *email;
@property(nonatomic, copy, readwrite) NSNumber *expirationMonth;
@property(nonatomic, copy, readwrite) NSNumber *expirationYear;
@property(nonatomic, copy, readwrite) NSNumber *currencyId;
@property(nonatomic, copy, readwrite) NSString *cardNumber;
@property(nonatomic, copy, readwrite) NSNumber *cvv2Code;
@property(nonatomic, copy, readwrite) NSString *comment;
@property(nonatomic, copy, readwrite) NSNumber *amount;
@property(nonatomic, copy, readwrite) NSString *externalTransactionId;
@property(nonatomic, copy, readwrite) NSNumber *createAlias;
@property(nonatomic, copy, readwrite) NSString *userTokenId;
@property(nonatomic, copy, readwrite) NSString *aliasName;
@property(strong, nonatomic) NSMutableDictionary *data;
@property(strong, nonatomic) NSString *deviceFingerprintId;
@property(strong, nonatomic) NSMutableDictionary *merchantDefinedData;


- (void) putData: (id) value forKey: (NSString *) key;
- (id) retrieveData: (NSString *) key;
- (NSData *) convertToJsonData;


@end
